'use strict';

angular.module('myApp.view1', ['ngRoute'])
    .config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.when('/view1', {
                templateUrl: 'view1/view1.html',
                controller: 'CarouselCtrl'
            });
        }])
    .controller('CarouselCtrl', ['$scope', 'getDataService', '$rootScope',
        function ($scope,
                  getDataService,
                  $rootScope) {
            $scope.slides = [];
            $rootScope.showTopMenu = false;

            getDataService.getData('slides.json')
                .then(function (response) {
                    $scope.slides = response.data;
                });
        }]);



