'use strict';
// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.view1',
    'myApp.view2'
]).
    config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/', {redirectTo: '/view1'})
                .otherwise({redirectTo: '/view1'});
        }])
    .run(['$rootScope', function ($rootScope) {
        $rootScope.transition = 500;
    }])
    .service('getDataService', ['$http',
        function ($http) {
            function getData(url) {
                return $http.get(url);
            }

            return {
                getData: getData
            };
        }])

    .directive('generalSettings', ['getDataService', function (getDataService) {
        return {
            templateUrl: 'settings/general.html',
            link: function ($scope) {
                getDataService.getData('general-settings.json')
                    .then(function (response) {
                        $scope.settings = response.data;
                    });

            },
            replace: true
        }
    }])
    .directive('socialSettings', ['getDataService', function (getDataService) {
        return {
            templateUrl: 'settings/social.html',
            link: function ($scope) {
                getDataService.getData('social-settings.json')
                    .then(function (response) {
                        $scope.social = response.data;
                    });
            },
            replace: true
        }
    }])
    .directive('options', ['getDataService', function (getDataService) {
        return {
            templateUrl: 'settings/settings.html',
            link: function ($scope) {
                $scope.settings.general = true;
            }
        }
    }])
    .controller('mainController', ['$scope', '$rootScope', '$timeout', 'getDataService',
        function ($scope, $rootScope, $timeout, getDataService) {
            $rootScope.showTopMenu = false;

            $scope.settings = [];


            $scope.showUserInfo = function () {
                $scope.active = true;
                $scope.showSettings = true;
            };

            $scope.closeSettings = function () {
                $scope.active = false;

                $timeout(function () {
                    $scope.showSettings = false;
                }, $rootScope.transition)
            };
        }]);
