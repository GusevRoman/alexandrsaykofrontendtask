'use strict';


angular.module('myApp.view2', ['ngRoute',
    'angular-carousel'])

    .config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.when('/view2', {
                templateUrl: 'view2/view2.html',
                controller: 'View2Ctrl'
            })
                .otherwise({redirectTo: '/view2'})
        }])

    .directive('topMenu', ['$timeout', '$rootScope',
        function ($timeout, $rootScope) {
            return {
                templateUrl: 'view2/topMenu.html',

                link: function ($scope, element, attrs) {
                    $('[whith="tooltip"]').tooltip();

                    $scope.menus = {
                        theme: false,
                        effects: false,
                        music: false,
                        imageLibrary: false,
                        businessData: false,
                        open: ''
                    };

                    $scope.openMenu = function (url) {
                        var openMenu = $scope.menus.open;

                        if (openMenu !== '' && openMenu !== url) {
                            closeMenu(openMenu);
                            $scope.menus.open = url;
                            $scope.menus[url] = true;
                        } else if (openMenu === url) {
                            closeMenu(openMenu);
                            $scope.menus.open = '';
                        } else {
                            $scope.menus.open = url;
                            $scope.menus[url] = true;
                        }

                        function closeMenu(menuToClose) {
                            $('.top.open').removeClass('active');
                            $timeout(function () {
                                $scope.menus[menuToClose] = false;
                            }, $rootScope.transition)
                        }
                    };

                }
            };
        }])

    .directive('allFrames', function () {
        return {
            templateUrl: 'view2/allFrames.html'
        }
    })

    .directive('sceneryMenu', ['$timeout',
    function ($timeout) {
        return {
            templateUrl: 'view2/scenery.html',
            link: function ($scope) {
                $timeout(function () {
                    $scope.active = true;
                }, 0);

            },
            replace: true
        }
    }])

    .directive('imageLibraryMenu', ['$timeout',
    function ($timeout) {
        return {
            templateUrl: 'view2/library.html',
            link: function ($scope) {
                $timeout(function () {
                    $scope.active = true;
                }, 0);
            },
            replace: true
        }
    }])

    .directive('scenery', ['getDataService', function (getDataService) {
        return {
            link: function ($scope, elem) {

                getDataService.getData('scenery.json')
                    .then(function (response) {
                        $scope.sceneries = response.data;
                    });
            }
        }
    }])

    .directive('slide',
    function () {
        return {
            compile: function compile(tElement, tAttrs, transclude) {
                return {
                    post: function ($scope, elem) {

                        //$scope.$watch(function () {
                        //    return elem.position().left
                        //}, function () {
                        //    if ($scope.$last) {
                        //        if (elem.position().left + elem.width() < window.innerWidth) {
                        //            $scope.$parent.carouselIndex = $scope.$index;
                        //        }
                        //    }
                        //});
                    }
                }
            }
        }
    })
    /*var parentElem = elem.parent();
     var lastItem = parentElem.find('.item:last');
     var positionLeft = $(lastItem).position().left;
     if (parentElem.width() > (positionLeft + lastItem.width())) {
     console.log(parentElem.width() - (positionLeft + lastItem.width()));
     parentElem.css({
     paddingLeft: parentElem.width() - (positionLeft + lastItem.width())
     });
     $scope.$parent.carouselIndex = parentElem.find('.item').length;
     } else {
     parentElem.css({
     paddingLeft: 0
     });
     }*/

    //window.innerWidth-$($0).position().left-197
    /*$($0).parent().css({
     paddingLeft: window.innerWidth-$($0).position().left-197
     })*/
    .directive('shareDirective', ['getDataService', function (getDataService) {
        return {
            templateUrl: 'view2/share.html',
            link: function ($scope) {
                getDataService.getData('social-settings.json')
                    .then(function (response) {
                        $scope.social = response.data;
                        $scope.social[0].options.forEach(function (item) {
                            if (item.btnName === 'remove') {
                                item.btnName = 'share'
                            }
                        });
                        $scope.social[1].options.forEach(function (item) {
                            if (item.btnName === 'remove') {
                                item.btnName = 'save'
                            }
                        });
                    });
            },
            replace: true
        }
    }])

    .service('getAspectRatio',
    function () {

        function getAspectRaio() {
            var aspectWidth = window.innerWidth;
            var aspectHeight = Math.floor(aspectWidth * 9 / 16);

            if (aspectHeight > window.innerHeight) {
                aspectHeight = window.innerHeight;
                aspectWidth = Math.floor(aspectHeight * 16 / 9);
            }

            return {
                width: aspectWidth,
                height: aspectHeight
            }
        }

        return {
            aspectRatio: getAspectRaio
        };
    })

    .controller('View2Ctrl', ['$scope', 'getDataService', '$rootScope', '$timeout', 'getAspectRatio',
        function ($scope,
                  getDataService, $rootScope, $timeout, getAspectRatio) {

            $scope.aspectRatio = getAspectRatio.aspectRatio();

            $(window).on('resize', function () {

                $scope.aspectRatio = getAspectRatio.aspectRatio();
                $scope.$digest();
            });

            $rootScope.showTopMenu = true;
            $scope.hideHeader = false;

            $scope.showMenu = function (menu) {
                $scope[menu] = true; //show All frames slide
                $scope.hideHeader = true;
                $scope.active = true; //show wrap
                $rootScope.showTopMenu = false;
            };

            $scope.close = function () {
                $rootScope.showTopMenu = true;
                $scope.active = false;
                $scope.hideHeader = false;

                $timeout(function () {
                    if ($scope.allFrames) {
                        $scope.allFrames = false;
                    } else if ($scope.share) {
                        $scope.share = false;
                    }
                }, $rootScope.transition)
            };

            $scope.frames = [];
            getDataService.getData('frames.json')
                .then(function (response) {
                    $scope.frames = response.data;
                });
        }]);